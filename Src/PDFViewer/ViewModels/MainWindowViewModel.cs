﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using Constants;
using Microsoft.Win32;
using Newtonsoft.Json;
using PDFViewer.Events;
using PDFViewer.Models;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;

namespace PDFViewer.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;

        public MainWindowViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;            

            RegisterCommands();

            PDFFileList = GenerateFileList();            
        }      

        private void RegisterCommands()
        {
            OnCloseCommand = new DelegateCommand(() =>
            {
                var json = JsonConvert.SerializeObject(PDFFileList.Select(pdfFile => pdfFile.FileLocation), Formatting.Indented);
                File.WriteAllText(PDFViewerConstants.PDFListingFileName, json);
            });

            OnLoadedCommand = new DelegateCommand(() =>
            {
                if (PDFFileList.Any()) SelectedPDFFile = PDFFileList.First();
            });

            AddFileCommand = new DelegateCommand(() =>
            {
                var dialog = new OpenFileDialog
                {
                    DefaultExt = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                    Filter = "PDF files (*.pdf)|*.pdf",
                    Multiselect = true,
                    Title = "Choose file(s) to add"
                };

                if(dialog.ShowDialog() is true)
                {
                    var uniquePaths = dialog.FileNames.Where(p => !PDFFileList.Any(pdfFile => pdfFile.FileLocation.Equals(p)));

                    foreach (var path in uniquePaths)
                    {
                        PDFFileList.Add(new PDFFile(path));
                    }
                }
            });

            RemoveFileCommand = new DelegateCommand(() =>
            {
                if(SelectedPDFFile == null)
                {
                    MessageBox.Show($"No File Selected!", "WARNING", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var result = MessageBox.Show($"Are you sure you want to remove {SelectedPDFFile.FileName}?", "WARNING", MessageBoxButton.YesNo, MessageBoxImage.Warning);

                if(result != MessageBoxResult.Yes) return;

                PDFFileList.Remove(SelectedPDFFile);

                if (PDFFileList.Any()) SelectedPDFFile = PDFFileList.First();                
            });

            ToggleToolbarCommand = new DelegateCommand(() =>
            {
                foreach (var pdfFile in PDFFileList)
                {
                    pdfFile.DisableToolbar = !pdfFile.DisableToolbar;
                }

                ToggleToolbarState = _selectedPDFFile.DisableToolbar;
                _eventAggregator.GetEvent<SelectedDocumentUpdatedEvent>().Publish(_selectedPDFFile);
            });
        }

        private ObservableCollection<PDFFile> GenerateFileList()
        {
            if(File.Exists(PDFViewerConstants.PDFListingFileName))
            {
                var toReturn = new ObservableCollection<PDFFile>();
                var paths = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText(PDFViewerConstants.PDFListingFileName));

                toReturn.AddRange(paths.Where(p => File.Exists(p)).Select(p => new PDFFile(p) { DisableToolbar = true }));
                ToggleToolbarState = true;
                return toReturn;
            }
            else
            {
                return new ObservableCollection<PDFFile>();
            }            
        }

        public ObservableCollection<PDFFile> PDFFileList { get; }

        private PDFFile _selectedPDFFile;
        public PDFFile SelectedPDFFile
        {
            get => _selectedPDFFile;
            set
            {
                _selectedPDFFile = value;
                RaisePropertyChanged(nameof(SelectedPDFFile));

                if (_selectedPDFFile == null) return;

                if (!File.Exists(_selectedPDFFile.FileLocation))
                {
                    MessageBox.Show($"{_selectedPDFFile.FileName} file was deleted! Removing {_selectedPDFFile} from list.", "WARNING", MessageBoxButton.OK, MessageBoxImage.Warning);
                    RemoveFileCommand.Execute();
                    return;
                }

                _eventAggregator.GetEvent<SelectedDocumentUpdatedEvent>().Publish(_selectedPDFFile);
            }
        }


        public DelegateCommand OnCloseCommand { get; set; }
        public DelegateCommand OnLoadedCommand { get; set; }
        public DelegateCommand AddFileCommand { get; set; }
        public DelegateCommand RemoveFileCommand { get; set; }
        public DelegateCommand ToggleToolbarCommand { get; set; }

        private bool _toggleToolbarState;
        public bool ToggleToolbarState
        {
            get => _toggleToolbarState;
            set
            {
                _toggleToolbarState = value;
                RaisePropertyChanged(nameof(ToggleToolbarState));
            }
        }
    }
}
