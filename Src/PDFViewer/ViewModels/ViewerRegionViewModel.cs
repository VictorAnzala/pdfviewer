﻿using PDFViewer.Events;
using PDFViewer.Models;
using Prism.Events;
using Prism.Mvvm;

namespace PDFViewer.ViewModels
{
    public class ViewerRegionViewModel : BindableBase
    {
        private readonly IEventAggregator _eventAggregator;

        public ViewerRegionViewModel(IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;

            RegisterEvents();
        }

        private void RegisterEvents()
        {
            _eventAggregator.GetEvent<SelectedDocumentUpdatedEvent>().Subscribe(HandleSelectedDocumentUpdatedEvent, ThreadOption.UIThread);
        }

        private void HandleSelectedDocumentUpdatedEvent(PDFFile file)
        {
            Address = "";
            Address = file.Address;
        }

        private string _address;
        public string Address
        {
            get => _address;
            set
            {
                _address = value;
                RaisePropertyChanged(nameof(Address));
            }
        }
    }
}
