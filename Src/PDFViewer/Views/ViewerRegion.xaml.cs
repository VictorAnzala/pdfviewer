﻿using System.Windows.Controls;

namespace PDFViewer.Views
{
    /// <summary>
    /// Interaction logic for ViewerRegion.xaml
    /// </summary>
    public partial class ViewerRegion : UserControl
    {
        public ViewerRegion()
        {
            InitializeComponent();
        }
    }
}
