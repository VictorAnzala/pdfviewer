﻿using System.IO;

namespace PDFViewer.Models
{
    public class PDFFile
    {
        public PDFFile(string location)
        {
            FileLocation = location;
            FileName = Path.GetFileName(location);
        }

        public string FileName { get; }
        
        public string FileLocation { get; }        

        public string Address
        {
            get
            {
                if (DisableToolbar)
                {
                    return $@"file:///{FileLocation}#toolbar=0";
                }
                else
                {
                    return FileLocation;
                }
            }
        }

        public bool DisableToolbar { get; set; }
    }
}
