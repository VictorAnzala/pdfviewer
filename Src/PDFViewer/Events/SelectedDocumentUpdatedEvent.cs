﻿using PDFViewer.Models;
using Prism.Events;

namespace PDFViewer.Events
{
    public class SelectedDocumentUpdatedEvent : PubSubEvent<PDFFile> { }    
}
