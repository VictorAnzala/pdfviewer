﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace PDFViewer.Converters
{
    public class InverseBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Inverter(value);
        }

        private bool Inverter(object value)
        {
            return value is bool b ? !b : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Inverter(value);
        }
    }
}
